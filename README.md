# LazPlanet 2021 Moon Package!

To see what's inside the package, see `src` directory.

To generate the zip file on the `dist` directory, run `./dist.sh` from a Linux machine.
