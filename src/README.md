# Hello Moon Dwellers!

Welcome to a small snippet of what LazPlanet is all about.

LazPlanet is a blog where loads of tutorials, articles, projects, tips and tricks live. All of the content is about Lazarus IDE and Free Pascal programming language. We have a website on earth with the URL https://lazplanet.gitlab.io which can be accessed with an internet browser to browse through those awesome content. The project is Free and Open Source (FOSS) and is available at https://gitlab.com/lazplanet/lazplanet.gitlab.io

Dare I say, there's no planet like LazPlanet! You're welcome there. When you arrive there, you become a "Lazarian"!


# File content

- lazplanet-logo.png: This is the old logo which has been used since 2013 when the blog started.
- lazplanet-proposed-logo.png: This is the new logo which has been designed for the new GitLab based website.
- lazarus-beginners-guide.pdf: PDF copy of an ebook for beginners written by Adnan Shameem. It's FOSS too, available at [GitLab](https://gitlab.com/lazplanet/lazarus-beginners-guide/).


# Special thanks

Thanks to [u/Valphon](https://www.reddit.com/user/Valphon/) for [allowing this](https://www.reddit.com/r/space/comments/l52iym/you_can_send_something_to_the_moon_for_free_im/) to be on the Moon.

And also to all the Lazarians who supported this project!
